<?php
namespace App\Service;

use App\Entity\Airport;
use App\Entity\Plane;
use App\Entity\Crewman;
use Symfony\Component\Form\FormInterface;

interface IPlaneService
{
    /**
     * @return Plane[]|iterable
     */
    public function getAllPlanes() : iterable;

    /**
     * @return Crewman[]|iterable
     */
    public function getAllCrewmen() : iterable;

    /**
     * @param int $crewmanId
     * @return Plane
     */
    public function getCrewmanById(int $crewmanId) : Crewman;

    /**
     * @param int $airportId
     * @return Plane
     */
    public function getAirportById(int $airportId) : Airport;

    /**
     * @param int $planeId
     * @return Plane
     */
    public function getPlaneById(int $planeId) : Plane;

    /**
     * @param Plane $onePlane
     */
    public function savePlane(Plane $onePlane) : void;

    /**
     * @param Airport $oneAirport
     */
    public function saveAirport(Airport $oneAirport) : void;

    /**
     * @param Crewman $oneCrewman
     */
    public function saveCrewman(Crewman $oneCrewman) : void;

    /**
     * @param int $planeId
     */
    public function removePlane(int $planeId) : void;

    /**
     * @param int $crewmanId
     */
    public function removeCrewman(int $crewmanId) : void;

    /**
     * @param int $airportId
     */
    public function removeAirport(int $airportId) : void;

    /**
     * @param Plane $onePlane
     * @return FormInterface
     */
    public function getPlaneForm(Plane $onePlane) : FormInterface;

    /**
     * @param Airport $oneAirport
     * @return FormInterface
     */
    public function getAirportForm(Airport $oneAirport) : FormInterface;

    /**
     * @param Crewman $oneCrewman
     * @return FormInterface
     */
    public function getCrewmanForm(Crewman $oneCrewman) : FormInterface;
}
