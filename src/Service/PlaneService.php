<?php
namespace App\Service;

use App\Entity\Airport;
use App\Entity\Plane;
use App\Entity\Crewman;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use PhpParser\Node\Stmt\Label;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PlaneService extends CrudService implements IPlaneService
{
    // ALT+INS, Override, __construct + Implement all
    public function __construct(EntityManagerInterface $em, FormFactoryInterface $formFactory)
    {
        parent::__construct($em, $formFactory);
    }

    public function getPlaneRepo(): EntityRepository
    {
        return $this->em->getRepository(Plane::class);
    }

    public function getCrewRepo(): EntityRepository
    {
        return $this->em->getRepository(Crewman::class);
    }

    public function getAirportRepo(): EntityRepository
    {
        return $this->em->getRepository(Airport::class);
    }


    public function getAllPlanes(): iterable
    {
        return $this->getPlaneRepo()->findAll();
        // WARNING! Query Iterator / Query builder with pagination
    }

    public function getAllCrewmen(): iterable
    {
        return $this->getCrewRepo()->findAll();
        // WARNING! Query Iterator / Query builder with pagination
    }

    public function getPlaneById(int $planeId): Plane
    {
        /** @var Plane|null $onePlane */
        $onePlane = $this->getPlaneRepo()->find($planeId);
        if ($onePlane == null){
            throw new NotFoundHttpException("NO PLANE FOUND");
        }
        return $onePlane;
    }

    public function getCrewmanById(int $crewmanId): Crewman
    {
        /** @var Crewman|null $oneCrewman */
        $oneCrewman = $this->getCrewRepo()->find($crewmanId);
        if ($oneCrewman == null){
            throw new NotFoundHttpException("NO CREWMAN FOUND");
        }
        return $oneCrewman;
    }

    public function getAirportById(int $airportId): Airport
    {
        /** @var Airport|null $oneAirport */
        $oneAirport = $this->getAirportRepo()->find($airportId);
        if ($oneAirport == null){
            throw new NotFoundHttpException("NO PLANE FOUND");
        }
        return $oneAirport;
    }

    public function removePlane(int $planeId): void
    {
        $crewInstance = $this->getAllCrewmen();
        $onePlane = $this->getPlaneById($planeId);

        foreach ($crewInstance as &$value) {
            if ($value->getPlaceOfService()->getPlaneId() == $onePlane->getPlaneId()){
                $this->em->remove($value);
            }
        }

        $this->em->remove($onePlane);
        $this->em->flush();
    }

    public function removeCrewman(int $crewmanId): void
    {
        $oneCrewman = $this->getCrewmanById($crewmanId);
        $this->em->remove($oneCrewman);
        $this->em->flush();
    }

    public function removeAirport(int $airportId): void
    {
        $planeInstance = $this->getAllPlanes();
        $oneAirport = $this->getAirportById($airportId);

        foreach ($planeInstance as &$value) {
            if (($value->getDestinationAirport()->getAirportId() == $oneAirport->getAirportId())
                || ($value->getDepartureLocation()->getAirportId() == $oneAirport->getAirportId())){
                $this->removePlane($value->getPlaneId());
            }
        }

        $this->em->remove($oneAirport);
        $this->em->flush();
    }

    public function savePlane(Plane $onePlane): void
    {
        $this->em->persist($onePlane);
        $this->em->flush();
    }

    public function saveAirport(Airport $oneAirport): void
    {
        $this->em->persist($oneAirport);
        $this->em->flush();
    }

    public function saveCrewman(Crewman $oneCrewman): void
    {
        $this->em->persist($oneCrewman);
        $this->em->flush();
    }

    public function getPlaneForm(Plane $onePlane): FormInterface
    {
        $form = $this->formFactory->createBuilder(FormType::class, $onePlane);
        $form->add("plane_type", TextType::class, [ "required"=>true ]);
        $form->add("passenger_capacity", NumberType::class, [ "required"=>true ]);
        $form->add("destination_airport", EntityType::class, [
            "class" => Airport::class,
            "choice_label"=>"airport_name",
            "choice_value"=>"airport_id"
        ]);
        $form->add("departure_location", EntityType::class, [
            "class" => Airport::class,
            "choice_label"=>"airport_name",
            "choice_value"=>"airport_id"
        ]);
        $form->add("SAVE", SubmitType::class);
        return $form->getForm();
    }

    public function getAirportForm(Airport $oneAirport): FormInterface
    {
        $form = $this->formFactory->createBuilder(FormType::class, $oneAirport);
        $form->add("airport_name", TextType::class, [ "required"=>true ]);
        $form->add("airport_location", TextType::class, [ "required"=>true ]);
        $form->add("airport_capacity", NumberType::class, [ "required"=>true ]);
        $form->add("SAVE", SubmitType::class);
        return $form->getForm();
    }

    public function getCrewmanForm(Crewman $oneCrewman): FormInterface
    {
        $form = $this->formFactory->createBuilder(FormType::class, $oneCrewman);
        $form->add("crewman_name", TextType::class, [ "required"=>true ]);
        $form->add("country_of_origin", TextType::class, [ "required"=>true ]);
        $form->add("job_title", TextType::class, [ "required"=>true ]);
        $form->add("crewman_age", NumberType::class, [ "required"=>true ]);
        $form->add("place_of_service", EntityType::class, [
            "class" => Plane::class,
            "choice_label"=>"plane_id",
            "choice_value"=>"plane_id"
        ]);
        $form->add("SAVE", SubmitType::class);
        return $form->getForm();
    }
}