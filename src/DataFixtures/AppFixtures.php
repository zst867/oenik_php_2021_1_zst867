<?php

namespace App\DataFixtures;

use App\Entity\Airport;
use App\Entity\Crewman;
use App\Entity\Plane;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\DBAL\Logging\DebugStack;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AppFixtures extends Fixture implements ContainerAwareInterface
{
    /** @var string */
    private $environment; // dev, test
    /** @var EntityManager */
    private $em;
    /** @var ContainerInterface */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
        $kernel = $this->container->get('kernel');
        if ($kernel) $this->environment = $kernel->getEnvironment();
    }

    public function load(ObjectManager $manager)
    {
        $this->em = $manager;

        $stackLogger = new DebugStack();
        $this->em->getConnection()->getConfiguration()->setSQLLogger($stackLogger);

        $airport1 = new Airport();
        $airport1->setAirportLocation("Budapest");
        $airport1->setAirportCapacity(150);
        $airport1->setAirportName("Ferihegy");
        $this->em->persist($airport1);

        $airport2 = new Airport();
        $airport2->setAirportLocation("Warsaw");
        $airport2->setAirportCapacity(200);
        $airport2->setAirportName("Warsaw Chopin");
        $this->em->persist($airport2);

        $airport3 = new Airport();
        $airport3->setAirportLocation("Berlin");
        $airport3->setAirportCapacity(250);
        $airport3->setAirportName("Berlin Tegel");
        $this->em->persist($airport3);
        $this->em->flush();

        $plane1 = new Plane();
        $plane1->setPlaneType("Boeing 737");
        $plane1->setPassengerCapacity(200);
        $plane1->setDepartureLocation($airport1);
        $plane1->setDestinationAirport($airport2);
        $this->em->persist($plane1);

        $plane2 = new Plane();
        $plane2->setPlaneType("Boeing 767");
        $plane2->setPassengerCapacity(100);
        $plane2->setDepartureLocation($airport1);
        $plane2->setDestinationAirport($airport3);
        $this->em->persist($plane2);

        $plane3 = new Plane();
        $plane3->setPlaneType("Airbus A330");
        $plane3->setPassengerCapacity(300);
        $plane3->setDepartureLocation($airport2);
        $plane3->setDestinationAirport($airport1);
        $this->em->persist($plane3);
        $this->em->flush();

        $crewman1 = new Crewman();
        $crewman1->setCrewmanName("Joe");
        $crewman1->setCrewmanAge(10);
        $crewman1->setCountryOfOrigin("Hungary");
        $crewman1->setPlaceOfService($plane1);
        $crewman1->setJobTitle("pilot");
        $this->em->persist($crewman1);

        $crewman2 = new Crewman();
        $crewman2->setCrewmanName("Bill");
        $crewman2->setCrewmanAge(20);
        $crewman2->setCountryOfOrigin("Germany");
        $crewman2->setPlaceOfService($plane2);
        $crewman2->setJobTitle("stewardess");
        $this->em->persist($crewman2);

        $crewman3 = new Crewman();
        $crewman3->setCrewmanName("Lily");
        $crewman3->setCrewmanAge(30);
        $crewman3->setCountryOfOrigin("Austria");
        $crewman3->setPlaceOfService($plane2);
        $crewman3->setJobTitle("pilot");
        $this->em->persist($crewman3);
        $this->em->flush();

        /*$user1 = new User();
        $user1->setFirstName("Bill");
        $user1->setLastName("Bill");
        $user1->setEmail("Bill@Bill");
        $user1->setPassword("123456");
        $user1->setRoles(["ROLE_USER"]);
        $this->em->persist($user1);

        $user2 = new User();
        $user2->setFirstName("Admin");
        $user2->setLastName("Admin");
        $user2->setEmail("Admin@Admin");
        $user2->setPassword("123456");
        $user2->setRoles(["ROLE_ADMIN"]);
        $this->em->persist($user2);
        $this->em->flush();*/


    }
}
