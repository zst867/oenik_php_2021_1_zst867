<?php
namespace App\Controller;

use App\DTO\UserQueryDto;
use App\Entity\Plane;
use App\Entity\Airport;
use App\Entity\Crewman;
use App\Service\IPlaneService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PlaneController
 * @package App\Controller
 * @Route(path="/planes/")
 */
class PlaneController extends AbstractController
{
    /** @var IPlaneService */
    private $planeService;
    /** @var FormFactoryInterface */
    private $formFactory;

    public function __construct(IPlaneService $planeService, FormFactoryInterface $formFactory)
    {
        $this->planeService = $planeService;
        $this->formFactory = $formFactory;
    }

    /**
     * @param Request $request
     * @return Response
     * @Route(name="planelist", path="list")
     */
    public function listAction(Request $request) : Response
    {
        $planes = $this->planeService->getAllPlanes();

        return $this->render('planes/plane_list.html.twig', ["planes" => $planes]);
    }

    /**
     * @param Request $request
     * @param int $planeId
     * @return Response
     * @Route(name="planeshow", path="show/{planeId}", requirements={ "planeId": "\d+" } )
     */
    public function showAction(Request $request, $planeId) : Response
    {
        $onePlane = $this->planeService->getPlaneById($planeId);
        return $this->render('planes/plane_show.html.twig', ["plane"=>$onePlane]);
    }

    /**
     * @param Request $request
     * @param int $airportId
     * @return Response
     * @Route(name="airportshow", path="airportshow/{airportId}", requirements={ "airportId": "\d+" } )
     */
    public function showActionAirport(Request $request, $airportId) : Response
    {
        $oneAirport = $this->planeService->getAirportById($airportId);
        return $this->render('planes/airport_show.html.twig', ["airport"=>$oneAirport]);
    }

    /**
     * @param Request $request
     * @param int $crewmanId
     * @return Response
     * @Route(name="crewmanshow", path="crewmanshow/{crewmanId}", requirements={ "crewmanId": "\d+" } )
     */
    public function showActionCrewman(Request $request, $crewmanId) : Response
    {
        $oneCrewman = $this->planeService->getCrewmanById($crewmanId);
        return $this->render('planes/crewman_show.html.twig', ["crewman"=>$oneCrewman]);
    }

    /**
     * @param Request $request
     * @param int $planeId
     * @return Response
     * @Route(name="planedel", path="del/{planeId}", requirements={ "planeId": "\d+" } )
     */
    public function delAction(Request $request, $planeId) : Response
    {
        $this->planeService->removePlane($planeId);
        $this->addFlash('notice', 'PLANE REMOVED');
        return $this->redirectToRoute('planelist');
    }

    /**
     * @param Request $request
     * @param int $airportId
     * @return Response
     * @Route(name="airportdel", path="airportdel/{airportId}", requirements={ "airportId": "\d+" } )
     */
    public function delActionAirport(Request $request, $airportId) : Response
    {
        $this->planeService->removeAirport($airportId);
        $this->addFlash('notice', 'AIRPORT REMOVED');
        return $this->redirectToRoute('planelist');
    }

    /**
     * @param Request $request
     * @param int $crewmanId
     * @return Response
     * @Route(name="crewmandel", path="crewmandel/{crewmanId}", requirements={ "crewmanId": "\d+" } )
     */
    public function delActionCrewman(Request $request, $crewmanId) : Response
    {
        $this->planeService->removeCrewman($crewmanId);
        $this->addFlash('notice', 'CREWMAN REMOVED');
        return $this->redirectToRoute('planelist');
    }

    /**
     * @param Request $request
     * @param int $planeId
     * @return Response
     * @Route(name="planeedit", path="edit/{planeId}", requirements={ "planeId": "\d+" } )
     */
    public function editAction(Request $request, $planeId = 0) : Response
    {
        if ($planeId) {
            $onePlane = $this->planeService->getPlaneById($planeId);
        } else {
            $onePlane = new Plane();
        }

        $form = $this->planeService->getPlaneForm($onePlane);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $this->planeService->savePlane($onePlane);
            $this->addFlash('notice', 'PLANE SAVED');
            return $this->redirectToRoute('planelist');
        }
        return $this->render('planes/planeedit.html.twig', ["form"=>$form->createView()]);
    }

    /**
     * @param Request $request
     * @param int $airportId
     * @return Response
     * @Route(name="airportedit", path="airportedit/{airportId}", requirements={ "airportId": "\d+" } )
     */
    public function editActionAirport(Request $request, $airportId = 0) : Response
    {
        if ($airportId) {
            $oneAirport = $this->planeService->getAirportById($airportId);
        } else {
            $oneAirport = new Airport();
        }

        $form = $this->planeService->getAirportForm($oneAirport);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $this->planeService->saveAirport($oneAirport);
            $this->addFlash('notice', 'AIRPORT SAVED');
            return $this->redirectToRoute('planelist');
        }
        return $this->render('planes/planeedit.html.twig', ["form"=>$form->createView()]);
    }

    /**
     * @param Request $request
     * @param int $crewmanId
     * @return Response
     * @Route(name="crewmanedit", path="crewmanedit/{crewmanId}", requirements={ "crewmanId": "\d+" } )
     */
    public function editActionCrewman(Request $request, $crewmanId = 0) : Response
    {
        if ($crewmanId) {
            $oneCrewman = $this->planeService->getCrewmanById($crewmanId);
        } else {
            $oneCrewman = new Crewman();
        }

        $form = $this->planeService->getCrewmanForm($oneCrewman);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $this->planeService->saveCrewman($oneCrewman);
            $this->addFlash('notice', 'CREWMAN SAVED');
            return $this->redirectToRoute('planelist');
        }
        return $this->render('planes/planeedit.html.twig', ["form"=>$form->createView()]);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route(name="readlist", path="readlist")
     */
    public function userQueryAction(Request $request) : Response
    {
        $planes = $this->planeService->getAllPlanes();
        return $this->render('planes/readonly_list.html.twig', ["planes" => $planes]);
    }



}
